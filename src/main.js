import Vue from 'vue'
import VueRouter from 'vue-router'
import App from './App.vue'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

//import routes
import Play from './components/GamePlay.vue'
import Question from './components/QuizQuestion.vue'
import GameOver from './components/GameOver.vue'
import Menu from './components/GameMenu.vue'

Vue.use(VueRouter);
Vue.use(BootstrapVue);
Vue.use(IconsPlugin);

//define routes
const routes = [
  { path: '/', component: Menu },
  { path: '/Play', name: '/Play', component: Play },
  { path: '/Question', component: Question },
  { path: '/Results', name: '/Results', component: GameOver },
  { path: '/Menu', component: Menu }
];

//create router
const router = new VueRouter({
  routes,
  mode: 'history'
})

Vue.config.productionTip = false

//the Cue app
new Vue({
  router,
  render: h => h(App)
}).$mount('#app');
