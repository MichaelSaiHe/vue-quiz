# Trivia-vue
A trivia app written in Vuejs. 

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

## Components included:

### GameMenu
Landing page for quiz, where user signs up and loads the rest of the program. 

### GameOver 
End game page, where total score is listed and a summary of every question and answers are listed (calls QuizResults). Also where options of new game or back to title is located.

### GamePlay
Fetches and creates questions, giving them one at a time until the list is exhausted. Currently gives 15 questions. 

### QuizQuestion
Naviations for questions, and shuffles answer options for the users. 

### QuizResult
Creates a list of every question and answers the user submitted and shows them which are right and wrong. 