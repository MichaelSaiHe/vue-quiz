FROM node:lts-alpine AS builder

ENV PORT 3000

WORKDIR /src
COPY package.json .
RUN npm install     
COPY . .

RUN npm run build
# /src/dist

FROM node:lts-alpine AS production

EXPOSE 3000/tcp
WORKDIR /app
RUN npm install express morgan connect-history-api-fallback
COPY index.js .
COPY --from=builder /src/dist ./dist


CMD [ "node", "index.js" ]  
